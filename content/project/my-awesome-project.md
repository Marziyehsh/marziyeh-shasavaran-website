+++
title = "My awesome project"
description = "Description of my awesome project."
date = 2014-11-18T02:13:50Z
author = "Marziyeh"
+++

## About my projects

I have been working in Iran for about six years after completing my bachelor's degree. I worked for a design company and was responsible for designing the plans and facades of buildings.The design and drawing work of the plan was done with AutoCAD software and the design of the building facade was done with 3D Max software.


![Facade-design.jpg.jpg](Facade-design.jpg)


